package kafkaproducer

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
	"github.com/kr/pretty"
)

type Instance struct{ producer *kafka.Producer }

func New(bootstrapServers []string) (*Instance, error) {
	hostname, err := os.Hostname()
	if err != nil {
		return nil, err
	}
	p, err := kafka.NewProducer(&kafka.ConfigMap{
		"bootstrap.servers": strings.Join(bootstrapServers, ","),
		"client.id":         hostname,
		"acks":              "all",
	})
	if err != nil {
		return nil, err
	}

	go func() {
		for e := range p.Events() {
			switch ev := e.(type) {
			case *kafka.Message:
				if ev.TopicPartition.Error != nil {
					log.Println("[kafkaproducer] delivery failed:", ev.TopicPartition)
				}
			}
		}
	}()

	return &Instance{p}, nil
}

func (i *Instance) Produce(topic, key string, value any, headers []kafka.Header) error {
	var keyb []byte
	if key != "" {
		keyb = []byte(key)
	}

	var valueb []byte
	switch v := value.(type) {
	case []byte:
		valueb = v
	case string:
		valueb = []byte(v)
	default:
		var err error
		valueb, err = json.Marshal(v)
		if err != nil {
			log.Printf("[kafkaproducer] error json marshal: %s", pretty.Sprint(v))
			panic(err)
		}
	}

	return printReturnErr(
		fmt.Sprintf("[%s]", topic),
		i.producer.Produce(
			&kafka.Message{
				TopicPartition: kafka.TopicPartition{
					Topic:     &topic,
					Partition: kafka.PartitionAny,
				},
				Key:     keyb,
				Value:   valueb,
				Headers: headers,
			},
			nil))
}

func (i *Instance) Close() { i.producer.Close() }

func printReturnErr(head string, err error) error {
	if err != nil {
		log.Printf("%s: %v", head, err)
	}
	return err
}
